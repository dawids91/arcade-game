package com.example.arcade_game.views;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.arcade_game.R;

public class GameInstructions extends Activity {

	TextView tvInstructions = null;
	Button btnInstructions = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_instructions);
		tvInstructions = (TextView)findViewById(R.id.tvInstructions);
		btnInstructions = (Button)findViewById(R.id.btnInstructions);
		btnInstructions.setOnClickListener(btnInstructions_listener);
	}
	
	private OnClickListener btnInstructions_listener = new OnClickListener(){
		@Override
		public void onClick(View v){
			finish();
		}
	};
}