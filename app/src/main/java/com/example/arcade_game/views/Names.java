package com.example.arcade_game.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.arcade_game.models.GameToast;
import com.example.arcade_game.R;
import com.example.arcade_game.models.UsersAdapter;
import com.example.arcade_game.database.User;
import com.example.arcade_game.database.Users_Helper;

import java.util.List;

public class Names extends Activity {

	private Button btnPlay = null;
	private EditText etUsername = null;
	public static String username = null;
	public final static String defaultUser = "USER";
	private Users_Helper db = null;
	private List<User> usersList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.names);
		btnPlay = (Button) findViewById(R.id.btnPlay);
		btnPlay.setOnClickListener(Play_listener);
		etUsername = (EditText) findViewById(R.id.etUsername);
		ListView listView = (ListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(listView_listener);
		db = new Users_Helper(this);
		usersList = db.getDistinctUsers();
		if (db.getUserCount() != 0) lvAdapter(listView);
	}

	private void lvAdapter(ListView listView) {
		UsersAdapter adapter = new UsersAdapter(this, usersList);
		listView.setAdapter(adapter);
	}

	OnClickListener Play_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			username = String.valueOf(etUsername.getText());
			username = username.trim();	//removing whitespaces
			if(username.isEmpty()) {
				username = defaultUser;
				new GameToast(getApplicationContext()).showBottom("Username wont be saved in Database, but 'USER'");
			}
			else {
				new GameToast(getApplicationContext()).showBottom("User will be created.");
			}
			Intent Countdown = new Intent(Names.this, Countdown.class);
			startActivity(Countdown);
			finish();
		}
	};
	AdapterView.OnItemClickListener listView_listener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			etUsername.setText(usersList.get(position).getUser());
			etUsername.selectAll(); //select to focus user on this element
		}
	};
}