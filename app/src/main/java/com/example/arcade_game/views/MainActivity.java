package com.example.arcade_game.views;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.arcade_game.models.GameToast;
import com.example.arcade_game.R;

public class MainActivity extends AppCompatActivity {

    private Button buttonSingleGame = null;
    private Button buttonSettings = null;
    private Button buttonStatistics = null;
    private Button buttonGameInstructions = null;
    private Button buttonGameDeveloper = null;
    private Button buttonExit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buttonSingleGame = (Button)findViewById(R.id.buttonSingleGame);
        buttonSingleGame.setOnClickListener(buttonSingleGame_listener);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonSettings.setOnClickListener(buttonSettings_listener);
        buttonStatistics = (Button)findViewById(R.id.buttonStatistics);
        buttonStatistics.setOnClickListener(buttonStatistics_listener);
        buttonGameInstructions = (Button) findViewById(R.id.buttonGameInstructions);
        buttonGameInstructions.setOnClickListener(buttonGameInstructions_listener);
        buttonGameDeveloper = (Button)findViewById(R.id.buttonGameDeveloper);
        buttonGameDeveloper.setOnClickListener(buttonGameDeveloper_listener);
        buttonExit = (Button)findViewById(R.id.buttonExit);
        buttonExit.setOnClickListener(buttonExit_listener);
    }
    private OnClickListener buttonSingleGame_listener = new OnClickListener(){
        @Override
        public void onClick(View v){
            Intent Names = new Intent(MainActivity.this, Names.class);
            startActivity(Names);
        }
    };
    private OnClickListener buttonSettings_listener = new OnClickListener(){
        @Override
        public void onClick(View v){
            Intent Settings = new Intent(MainActivity.this, Settings.class);
            startActivity(Settings);
        }
    };
    private OnClickListener buttonStatistics_listener = new OnClickListener(){
        @Override
        public void onClick(View v){
            Intent Statistics = new Intent(MainActivity.this, Statistics.class);
            startActivity(Statistics);
        }
    };
    private OnClickListener buttonGameInstructions_listener = new OnClickListener(){
        @Override
        public void onClick(View v){
            Intent GameInstructions = new Intent(MainActivity.this, GameInstructions.class);
            startActivity(GameInstructions);
        }
    };
    private OnClickListener buttonGameDeveloper_listener = new OnClickListener(){
        @Override
        public void onClick(View v){
            new GameToast(getApplicationContext()).showBottom("Game developed by Dawid Siupa");
        }
    };
    private OnClickListener buttonExit_listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }

    };
}