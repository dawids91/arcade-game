package com.example.arcade_game.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;

import com.example.arcade_game.models.BallFactory;
import com.example.arcade_game.models.GameCountDownTimer;
import com.example.arcade_game.models.GamePaint;
import com.example.arcade_game.models.GameToast;
import com.example.arcade_game.models.RefreshBitmapThread;
import com.example.arcade_game.models.Score;

public class SingleGame extends Activity implements OnTouchListener {

	private SingleGameViewer singleGameView;
	private float xTouched;
	private float yTouched;
	private GameCountDownTimer countDownTimerGame;
	public BallFactory ballFactory;
	private RefreshBitmapThread wait;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(singleGameView = new SingleGameViewer(this));
		singleGameView.setOnTouchListener(this);
	}

	public class SingleGameViewer extends View {

		public SingleGameViewer(Context context) {
			super(context);
		}

		public void reloadBitmap() {
			ballFactory = new BallFactory(SingleGame.this, this);
			wait.interrupt();
			wait = new RefreshBitmapThread(ballFactory,this);
			wait.start();
		}

		@Override
		protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
			super.onLayout(changed, left, top, right, bottom);	//method called at initializing the game, before onDraw
			countDownTimerGame = new GameCountDownTimer(getApplicationContext(), this, SingleGame.this);
			ballFactory = new BallFactory(SingleGame.this, this);
			wait = new RefreshBitmapThread(ballFactory, this);
			wait.start();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			GamePaint gameView = new GamePaint(this, canvas);
			gameView.drawBackground(getApplicationContext());
			gameView.drawPoints();
			drawTimeLeft(gameView);
			gameView.drawCoordinateX(xTouched);
			gameView.drawCoordinateY(yTouched);
			gameView.drawBitmap(ballFactory);
		}

		private void drawTimeLeft(GamePaint gameView) {
			if (countDownTimerGame.timeLeft > 5) gameView.drawTimeLeft();
			else gameView.drawTimeLeftWithWarning();
		}
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		xTouched = event.getX();
		yTouched = event.getY();
		if (ballFactory.isHit(xTouched, yTouched)) {
			Score.calculatePoints(ballFactory.getRandomBallID());
			singleGameView.reloadBitmap();
			singleGameView.postInvalidate();
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		countDownTimerGame.cancel();
		new GameToast(this).showBottom("Points not saved to the Database. Game interrupted");
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.single_game_menu, menu);
//		return true;
//	}
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//			case R.id.exit:
//				onBackPressed();
//				return true;
//			case R.id.help:
//				toastToShow("Tap on the balls as quickly as you can, but don't touch the red BOMB!", Gravity.TOP, getApplicationContext());
//				return true;
//			default:
//				return super.onOptionsItemSelected(item);
//		}
//	}
}