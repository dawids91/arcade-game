package com.example.arcade_game.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;

import com.example.arcade_game.R;

public class Countdown extends Activity implements OnTouchListener {

	private CountDownViewer countDownView = null;
	private int counter = 0;
	private Paint paint = null;
	private Bitmap background = null;
	private CountDownTimer timer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(countDownView = new CountDownViewer(this));
		countDownView.setOnTouchListener(this);	//skip countdown when touched
		background = BitmapFactory.decodeResource(getResources(), R.drawable.background1);
		paint = new Paint();
		drawWithProperties(Color.GREEN, 150, Typeface.SERIF, Typeface.NORMAL, paint);
	}

	class CountDownViewer extends View {
		public CountDownViewer(Context context) {
			super(context);
			timer = new CountDownTimer(4000, 1000) {
				@Override
				public void onTick(long millisUntilFinished) {
					counter = (int) ((millisUntilFinished / 1000));
					countDownView.invalidate();
				}
				@Override
				public void onFinish() {
					Intent SingleGame = new Intent(Countdown.this, SingleGame.class);
					startActivity(SingleGame);
					finish();
				}
			}.start();
		}
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			canvas.drawBitmap(background, 0, 0, paint);
			drawTextCenter("GET READY!", String.valueOf(counter), canvas);
		}
	}
	private void drawTextCenter(String text1, String text2, Canvas canvas) {
		final float maxX = countDownView.getWidth();
		final float maxY = countDownView.getHeight();
		float txtHeight = paint.getTextSize();
		float txtWidth = paint.measureText(text1);
		canvas.drawText(text1, (maxX / 2) - (txtWidth / 2), (maxY / 2), paint);
		txtWidth = paint.measureText(text2);
		canvas.drawText(text2, (maxX / 2) - (txtWidth / 2), (maxY / 2) + (txtHeight), paint);
	}
	private void drawWithProperties(int color, int textSize, Typeface font, int fontStyle, Paint paint) {
		paint.setColor(color);
		paint.setTextSize(textSize);
		paint.setTypeface(Typeface.create(font, fontStyle));
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		Intent SingleGame = new Intent(Countdown.this, SingleGame.class);
		startActivity(SingleGame);
		timer.cancel();
		finish();
		return false;
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		timer.cancel();
		finish();
	}
}