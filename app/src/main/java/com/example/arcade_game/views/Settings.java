package com.example.arcade_game.views;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.example.arcade_game.models.GameSharedPreferences;
import com.example.arcade_game.R;

public class Settings extends Activity {

	private RadioButton rdBackgroundGray = null;
	private RadioButton rdBackgroundGreen = null;
	private RadioButton rdBackgroundViolet = null;
	private RadioGroup rdGroupTime = null;
	private RadioButton rdTime1 = null;
	private RadioButton rdTime2 = null;
	private int selectedMapColor = 0;
	private int selectedPlaytime = 0;
	private boolean mapColorChanged = false;
	private boolean playtimeChanged = false;
	//* colors *//
	private static final int COLOR_GREEN = Color.rgb(68, 148, 68);
	private static final int COLOR_VIOLET = Color.rgb(161, 128, 182);
	private static final int COLOR_GRAY = Color.GRAY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
		RadioGroup rdGroupBackground = (RadioGroup) findViewById(R.id.rdGroupBackground); //first RadioGroup
    	rdGroupBackground.setOnCheckedChangeListener(background_listener); //and their listener
        rdBackgroundGray = (RadioButton) findViewById(R.id.rdBackgroundGray);
        rdBackgroundGreen = (RadioButton) findViewById(R.id.rdBackgroundGreen);
        rdBackgroundViolet = (RadioButton) findViewById(R.id.rdBackgroundViolet);
        rdGroupTime = (RadioGroup) findViewById(R.id.rdGroupTime); //second RadioGroup
        rdGroupTime.setOnCheckedChangeListener(time_listener); //and their listener
        rdTime1 = (RadioButton) findViewById(R.id.rdTime1);
        rdTime2 = (RadioButton) findViewById(R.id.rdTime2);
    	Button btnSettingsOK = (Button)findViewById(R.id.btnSettingsOK);
    	btnSettingsOK.setOnClickListener(btnSettingsOK_listener);
		selectedMapColor = GameSharedPreferences.getMapColor(this);
		selectedPlaytime = GameSharedPreferences.getPlayTime(this);
		setRadioButtons();
	}

	private OnCheckedChangeListener time_listener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			checkedId = rdGroupTime.getCheckedRadioButtonId();
			switch (checkedId) {
				case R.id.rdTime2:
					selectedPlaytime = 60000; break;
				default:
					selectedPlaytime = 0; break;
			}
			playtimeChanged = true;
		}
	};

	private OnCheckedChangeListener background_listener = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup rdGroupBackground, int checkedId) {
			checkedId = rdGroupBackground.getCheckedRadioButtonId();
			switch (checkedId) {
				case R.id.rdBackgroundGreen:
					selectedMapColor = COLOR_GREEN; break;
				case R.id.rdBackgroundViolet:
					selectedMapColor = COLOR_VIOLET; break;
				default:
					selectedMapColor = COLOR_GRAY; break;
			}
			mapColorChanged = true;
		}
	};

	private void setRadioButtons() {
		if(selectedMapColor == COLOR_GREEN) rdBackgroundGreen.setChecked(true); //here must use ifs instead of switch statement because in case condition must be a constant expression, but (Color.rgb(x, y, z)) isn't
		else if(selectedMapColor == COLOR_VIOLET) rdBackgroundViolet.setChecked(true);
		else rdBackgroundGray.setChecked(true);
		switch(selectedPlaytime) {
			case 60000:
				rdTime2.setChecked(true); break;
			default: //30000
				rdTime1.setChecked(true); break;
		}
	}

	private OnClickListener btnSettingsOK_listener = new OnClickListener(){
		@Override
		public void onClick(View v){
			onBackPressed();
		}
	};

	@Override
	public void onBackPressed() {
		if (mapColorChanged) GameSharedPreferences.setMapColor(this, selectedMapColor);
		if (playtimeChanged) GameSharedPreferences.setPlayTime(this, selectedPlaytime);
		super.onBackPressed();
	}
}