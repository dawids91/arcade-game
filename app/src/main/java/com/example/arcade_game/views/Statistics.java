package com.example.arcade_game.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.example.arcade_game.models.GameToast;
import com.example.arcade_game.R;
import com.example.arcade_game.models.UsersStatisticsAdapter;
import com.example.arcade_game.database.Users_Helper;

import java.util.Collections;
import java.util.List;

public class Statistics extends Activity {

	private Button btnClearDatabase = null;
	private Button btnBack = null;
	private Users_Helper db = null;
	private ListView lv30seconds, lv60seconds = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.statistics);
		lv30seconds = (ListView) findViewById(R.id.listView);
		lv60seconds = (ListView) findViewById(R.id.listView2);
		btnClearDatabase = (Button) findViewById(R.id.btnClearDatabase);
		btnClearDatabase.setOnClickListener(btnClearDatabase_listener);
		btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(btnBack_listener);
		db = new Users_Helper(this);
		presentData();
	}

	private void presentData() {
		if (db.getUserCount() != 0) {
			//both toasts only for testing purposes
			lvAdapter(lv30seconds, db.getUsersWithPlaytime(30000));
			//SingleGame.toastToShow("Cols = " + db.columnCount + "  /  Rows = " + db.getUsersWithPlaytime(30000).size(), Gravity.CENTER, this);   //*** TESTING PURPOSES ***//
			lvAdapter(lv60seconds, db.getUsersWithPlaytime(60000));
			//SingleGame.toastToShow("Cols = " + db.columnCount + "  /  Rows = " + db.getUsersWithPlaytime(60000).size(), Gravity.CENTER, this);   //*** TESTING PURPOSES ***//
		}
		else new GameToast(this).showCenter("Database doesn't exist!");
	}

	private void lvAdapter(ListView listView, List users) {
		UsersStatisticsAdapter adapter = new UsersStatisticsAdapter(this, users);
		listView.setAdapter(adapter);
	}

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					db.clearTables();
					lvAdapter(lv30seconds, Collections.emptyList());
					lvAdapter(lv60seconds, Collections.emptyList());
					new GameToast(getApplicationContext()).showCenter("Database cleared!");
					break;
				case DialogInterface.BUTTON_NEGATIVE:
					//no button clicked - no action required
					break;
			}
		}
	};

	private OnClickListener btnClearDatabase_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (db.getUserCount() != 0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(Statistics.this);
				builder.setMessage("Are you sure to delete the database?").setPositiveButton("Yes", dialogClickListener)
						.setNegativeButton("No", dialogClickListener).show();
			} else {
				new GameToast(getApplicationContext()).showCenter("Database doesn't exist!");
			}
		}
	};
	private OnClickListener btnBack_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			finish();
		}
	};
}