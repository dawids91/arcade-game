package com.example.arcade_game.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class Users_Helper extends SQLiteOpenHelper {

	public static int columnCount = 0;
	
	public Users_Helper(Context context) {
		super(context, UsersContract.DATABASE_NAME, null, UsersContract.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL(UsersContract.UsersEntry.CREATE_TABLE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			db.execSQL(UsersContract.UsersEntry.DELETE_TABLE);
			onCreate(db);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void clearTables() {
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			onUpgrade(db, 1, 1);
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (db != null && db.isOpen())
				db.close(); // Closing database connection
		}
	}

	// ---User
	public boolean addUser(User user) {
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(UsersContract.USER, user.getUser());
			values.put(UsersContract.POINTS, user.getPoints());
			values.put(UsersContract.DATE, user.getDate());
			values.put(UsersContract.PLAYTIME, user.getPlaytime());
			// inserting a row
			db.insert(UsersContract.UsersEntry.TABLE_NAME, null, values);
			return true;
		} catch (SQLiteException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (db != null && db.isOpen())
				db.close(); // closing database connection
		}
	}

	public int updateUser(User user) {
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(UsersContract.USER, user.getUser());
			values.put(UsersContract.POINTS, user.getPoints());
			values.put(UsersContract.DATE, user.getDate());
			values.put(UsersContract.PLAYTIME, user.getPlaytime());
			// updating row
			return db.update(UsersContract.UsersEntry.TABLE_NAME, values, BaseColumns._ID + " = ?",
					new String[] { String.valueOf(user.get_id()) });
		} catch (SQLiteException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (db != null && db.isOpen())
				db.close(); // closing database connection
		}
	}

	public String getUserName(int id) {
		SQLiteDatabase db = null;
		String userName = null;
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_USER, new String[]{String.valueOf(id)});
			if (cursor != null && cursor.moveToFirst())
				userName = cursor.getString(1);
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (db != null && db.isOpen())
				db.close(); // closing database connection
            return userName;
		}
	}

	public int getUserId(String userName) {
		SQLiteDatabase db = null;
		int id = 0;
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_USER_ID, new String[] { String.valueOf(userName) });
			if (cursor != null && cursor.moveToFirst())
				id = cursor.getInt(0);
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (db != null && db.isOpen())
				db.close(); // closing database connection
            return id;
		}
	}

	public User getUserWithNameAndPlaytime(String username, int playtime) {
		SQLiteDatabase db = null;
        User user = null;
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_USER_WITH_NAME_AND_PLAYTIME, new String[] {username, String.valueOf(playtime)});
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    user = new User(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getInt(4));
                } while (cursor.moveToNext());
            }
		} catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (db != null && db.isOpen())
                db.close(); // closing database connection
            return user;
        }
	}

	public List<User> getUsersWithPlaytime(int playtime) {
		SQLiteDatabase db = null;
		List<User> userList = new ArrayList<User>();
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_USERS_WITH_PLAYTIME, new String[] { String.valueOf(playtime) });
			columnCount = cursor.getColumnCount();
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					User user = new User();
					user.set_id(cursor.getInt(0));
					user.setUser(cursor.getString(1));
					user.setPoints(cursor.getInt(2));
					user.setDate(cursor.getString(3));
					user.setPlaytime(cursor.getInt(4));
					// Adding contact to list
					userList.add(user);
				} while (cursor.moveToNext());
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
		    if (db != null && db.isOpen())
            db.close(); // closing database connection
            return userList;
        }
	}

    public List<User> getDistinctUsersWithRecords(int playtime) {
        SQLiteDatabase db = null;
        List<User> userList = new ArrayList<User>();
        try {
            db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_DISTINCT_USERS_WITH_PLAYTIME, new String[] { String.valueOf(playtime) });
            columnCount = cursor.getColumnCount();
            // looping through all rows and adding to list
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    User user = new User();
                    user.setUser(cursor.getString(0));
                    user.setPoints(cursor.getInt(1));
                    user.setDate(cursor.getString(2));
                    // Adding contact to list
                    userList.add(user);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (db != null && db.isOpen())
                db.close(); // closing database connection
            return userList;
        }
    }

	public List<User> getDistinctUsers() {
		SQLiteDatabase db = null;
		List<User> userList = new ArrayList<User>();
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_DISTINCT_USERS, null);
			columnCount = cursor.getColumnCount();
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					User user = new User();
					user.setUser(cursor.getString(0));
					// Adding contact to list
					userList.add(user);
				} while (cursor.moveToNext());
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			if (db != null && db.isOpen())
				db.close(); // closing database connection
			return userList;
		}
	}

	public List<User> getAllUser() {
		SQLiteDatabase db = null;
		List<User> userList = new ArrayList<User>();
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.GET_ALL_USER, null);
			columnCount = cursor.getColumnCount();
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					User user = new User();
					user.set_id(cursor.getInt(0));
					user.setUser(cursor.getString(1));
					user.setPoints(cursor.getInt(2));
					user.setDate(cursor.getString(3));
					user.setPlaytime(cursor.getInt(4));
					// Adding contact to list
					userList.add(user);
				} while (cursor.moveToNext());
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
		    if (db != null && db.isOpen())
            db.close(); // closing database connection
            return userList; // return contact list
        }
	}

	public int getUserCount() {
	    SQLiteDatabase db = null;
		try {
			db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(UsersContract.UsersEntry.COUNT_QUERY, null);
			int count = cursor.getCount();
			cursor.close();
            return count;
		} catch (SQLiteException e) {
			e.printStackTrace();
			return 0;
		} finally {
            if (db != null && db.isOpen())
                db.close(); // closing database connection
        }
	}

	public void deleteContact(User user) {
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			db.delete(UsersContract.UsersEntry.TABLE_NAME, BaseColumns._ID + " = ?", new String[] { String.valueOf(user.get_id()) });
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            if (db != null && db.isOpen())
                db.close(); // closing database connection
		}
	}
}