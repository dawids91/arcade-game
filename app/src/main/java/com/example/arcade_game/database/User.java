package com.example.arcade_game.database;

public class User {
	private int _id;
	private String user;
	private int points;
	private String date;
	private int playtime;

	public User() {}
	public User(String user, int points, String date, int playtime) {
		this.user = user;
		this.points = points;
		this.date = date;
		this.playtime = playtime;
	}
	public User(int _id, String user, int points, String date, int playtime) {
		this._id = _id;
		this.user = user;
		this.points = points;
		this.date = date;
		this.playtime = playtime;
	}
	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getPlaytime() {
		return playtime;
	}
	public void setPlaytime(int playtime) {
		this.playtime = playtime;
	}
}