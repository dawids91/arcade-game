package com.example.arcade_game.database;

import android.provider.BaseColumns;

public class UsersContract {
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "Database.db";
	private static final String TEXT_TYPE = " TEXT";
	private static final String INT_TYPE = " INT";
    private static final String COMMA_SEP = ",";

	//---columns
	public final static String USER = "User";
	public final static String POINTS = "Points";
	public final static String DATE = "Date";
	public final static String PLAYTIME = "Playtime";
    
    public UsersContract() {}
    public static class UsersEntry implements BaseColumns {
    	public UsersEntry(){}
		public final static String TABLE_NAME = "Users";
		public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY," +
                USER + TEXT_TYPE + COMMA_SEP + POINTS + INT_TYPE + COMMA_SEP + DATE + TEXT_TYPE + COMMA_SEP + PLAYTIME + INT_TYPE + ")";
		public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		public static final String COUNT_QUERY = "SELECT * FROM " + TABLE_NAME;
		public static final String GET_USER =  "SELECT " + USER + " FROM " + TABLE_NAME + " WHERE " + _ID + " = ?" ;
		public static final String GET_USER_ID =  "SELECT " + _ID + " FROM " + TABLE_NAME + " WHERE " + USER + " = ?" ;
		public static final String GET_ALL_USER = "SELECT * FROM " + TABLE_NAME;
		public static final String GET_POINTS =  "SELECT " + POINTS + " FROM " + TABLE_NAME + " WHERE " + _ID + " = ?" ;
		public static final String GET_USERS_WITH_PLAYTIME = "SELECT * FROM " + TABLE_NAME + " WHERE " + PLAYTIME + " = ?" ;
		public static final String GET_USER_WITH_NAME_AND_PLAYTIME = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER + " = ? AND " + PLAYTIME + " = ?";
		public static final String GET_DISTINCT_USERS_WITH_PLAYTIME = "SELECT DISTINCT(" + USER + ")" + COMMA_SEP + " MAX(" + POINTS + ") " + COMMA_SEP + DATE + " FROM " + TABLE_NAME + " WHERE " + PLAYTIME + " = ? GROUP BY " + USER;
		public static final String GET_DISTINCT_USERS = "SELECT DISTINCT(" + USER + ")" + " FROM " + TABLE_NAME;
		public static final String GET_USER_RECORD = "SELECT MAX(" + POINTS + ") FROM " + TABLE_NAME + " WHERE " + USER + " = ?";
    }
}