package com.example.arcade_game.models;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class GameToast extends Toast {

    private Context passedContext;

    public GameToast(Context context) {
        super(context);
        this.passedContext = context;
    }

    public void showBottom(String text) {
        Toast toast = this.makeText(passedContext, text, LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    public void showCenter(String text) {
        Toast toast = this.makeText(passedContext, text, LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}