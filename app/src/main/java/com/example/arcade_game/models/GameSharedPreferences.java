package com.example.arcade_game.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

import static android.content.Context.MODE_PRIVATE;

public class GameSharedPreferences {

    private static final String PLAYTIME_SETTING_NAME = "playtime";
    private static final String MAPCOLOR_SETTING_NAME = "mapColor";
    private static final int DEFAULT_PLAYTIME = 30000;                                              // 3 seconds
    private static final int DEFAULT_MAP_COLOR = Color.GRAY;

    public static void setPlayTime(Context context, int playtime) {
        if (playtime == 0) {
            playtime = DEFAULT_PLAYTIME;
        }
        SharedPreferences preferences = context.getSharedPreferences(PLAYTIME_SETTING_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PLAYTIME_SETTING_NAME, playtime);
        editor.commit();
    }

    public static void setMapColor(Context context, int mapColor) {
        if (mapColor == 0) {
            mapColor = DEFAULT_MAP_COLOR;
        }
        SharedPreferences preferences = context.getSharedPreferences(MAPCOLOR_SETTING_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(MAPCOLOR_SETTING_NAME, mapColor);
        editor.commit();
    }

    public static int getPlayTime(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PLAYTIME_SETTING_NAME, MODE_PRIVATE);
        return preferences.getInt(PLAYTIME_SETTING_NAME, DEFAULT_PLAYTIME);
    }

    public static int getMapColor(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MAPCOLOR_SETTING_NAME, MODE_PRIVATE);
        return preferences.getInt(MAPCOLOR_SETTING_NAME, DEFAULT_MAP_COLOR);
    }
}
