package com.example.arcade_game.models;

public class Score {

    public static int points = 0;

    public Score() {}

    public static void calculatePoints(int catchedBall) {
        if (catchedBall == BallFactory.BAD_BALL_ID) {
            if (points >= 3) points -= 3;                                                           //* points cant be negative *//
            else points = 0;
        }
        else if (catchedBall == BallFactory.GOOD_BALL_ID) points += 3;
        else points++;
    }
}
