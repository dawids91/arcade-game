package com.example.arcade_game.models;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;

import com.example.arcade_game.views.Names;
import com.example.arcade_game.views.SingleGame;
import com.example.arcade_game.views.SingleGame.SingleGameViewer;
import com.example.arcade_game.database.User;
import com.example.arcade_game.database.Users_Helper;

public class GameCountDownTimer extends CountDownTimer {

    private SingleGameViewer singleGameView;
    private SingleGame singleGameActivity;
    private static final int refreshViewTime = 1000;                                                //* 1 second *//
    private int playingTime;
    public static int timeLeft;
    public static boolean refreshTime = false;

    public GameCountDownTimer(Context context, SingleGameViewer singleGameView, SingleGame singleGameActivity) {
        super(GameSharedPreferences.getPlayTime(context), refreshViewTime);                         //* long millisInFuture, long countDownInterval *//
        playingTime = GameSharedPreferences.getPlayTime(context);
        this.singleGameView = singleGameView;
        this.singleGameActivity = singleGameActivity;
        this.start();
    }

    @Override
    public void onTick(long millisUntilFinished) {
        timeLeft = (int) ((millisUntilFinished / 1000));                                            //* converted to seconds *//
        refreshTime = true;
        singleGameView.invalidate();
    }

    @Override
    public void onFinish() {
        insertPointsAndFinish();
    }

    private void insertPointsAndFinish() {
        Users_Helper db = new Users_Helper(singleGameView.getContext());
        User userSaved = db.getUserWithNameAndPlaytime(Names.username, playingTime);
        if (userSaved != null) { //current username is saved
            if (Score.points < userSaved.getPoints()) new GameToast(singleGameView.getContext()).showBottom("Your record wasn't beaten, you need " + (userSaved.getPoints() - Score.points) + " more. Total achieved " + Score.points + " points.");
            else { //record was beaten
                new GameToast(singleGameView.getContext()).showBottom("User updated. You broke your record by " + (Score.points - userSaved.getPoints()) + " and achieved " + Score.points + " points.");
                userSaved.setPoints(Score.points);
                userSaved.setDate(PlayingDate.getTodayDate());
                db.updateUser(userSaved);
            }
        } else {
            if (db.addUser(new User(Names.username, Score.points, PlayingDate.getTodayDate(), playingTime)))
                new GameToast(singleGameView.getContext()).showBottom("Points saved to the database. You achieved " + Score.points + " points.");
            else
                new GameToast(singleGameView.getContext()).showBottom("An error occurred with writing points to the database.");
        }
        Intent Statistics = new Intent(singleGameActivity, com.example.arcade_game.views.Statistics.class);
        singleGameActivity.startActivity(Statistics);
        singleGameActivity.finish();
    }
}
