package com.example.arcade_game.models;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.arcade_game.R;
import com.example.arcade_game.views.SingleGame;

import java.util.Random;

public class BallFactory {

    public static final int NORMAL_BALL_ID = 0;
    public static final int SILVER_BALL_ID = 2;
    public static final int GOOD_BALL_ID = 3;
    public static final int BAD_BALL_ID = 4;
    private static final int BALL_COUNT = 5;
    private Bitmap[] balls = null;
    private Bitmap currentBall;
    private int currentBallID;
    private float[] xy;
    private SingleGame.SingleGameViewer viewer;

    public BallFactory(Activity activity, SingleGame.SingleGameViewer viewer) {
        generateBallsBitmaps(activity);
        this.currentBallID = randomBall();
        this.currentBall = balls[this.currentBallID];
        this.viewer = viewer;
        this.xy = generateCoordinates(this.currentBall);
    }

    public boolean isHit(float x, float y) {
        if (x >= xy[0] && x <= (xy[0] + getBitmapWidth(currentBall)) && y >= xy[1] && y <= (xy[1] + getBitmapHeight(currentBall))) {
            return true;
        } else return false;
    }

    private void generateBallsBitmaps(Activity activity) {
        balls = new Bitmap[5];
        balls[0] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ball1);
        balls[1] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ball2);
        balls[2] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ballsilver);
        balls[3] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ball3good);
        balls[4] = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ball4bad);
    }

    public Bitmap getRandomBall() {
        return this.currentBall;
    }

    public int getRandomBallID() {
        return this.currentBallID;
    }

    public float[] getCoordinates() {
        return this.xy;
    }

    private int randomBall() {
        Random randomize = new Random();
        switch (randomize.nextInt(5)) { //BALL_ID = [0-4]
            case (0):
                return NORMAL_BALL_ID;
            case (1):	//normal ball should occur mostly
                return NORMAL_BALL_ID;
            case (2):
                return SILVER_BALL_ID;
            case (3):
                return GOOD_BALL_ID;
            default:
                return BAD_BALL_ID;
        }
    }

    private float[] generateCoordinates(Bitmap bitmap) {
        Random random = new Random();
        float x = random.nextFloat() * (getViewerWidth() - getBitmapWidth(bitmap));
        float y = random.nextFloat() * (getViewerHeight() - getBitmapHeight(bitmap));
        return new float[] {x, y};
    }

    private float getViewerWidth() {
        return this.viewer.getWidth();
    }

    private float getViewerHeight() {
        return this.viewer.getHeight();
    }

    private float getBitmapWidth(Bitmap bitmap) {
        return bitmap.getWidth();
    }

    private float getBitmapHeight(Bitmap bitmap) {
        return bitmap.getHeight();
    }
}
