package com.example.arcade_game.models;

import com.example.arcade_game.views.SingleGame;

public class RefreshBitmapThread extends Thread {

    private final int TIME_LONG = 900;
    private final int TIME_MEDIUM = 750;
    private final int TIME_SHORT = 600;
    private int randomBallID;
    private SingleGame.SingleGameViewer viewer;

    public RefreshBitmapThread(BallFactory ballFactory, SingleGame.SingleGameViewer viewer) {
        this.randomBallID = ballFactory.getRandomBallID();
        this.viewer = viewer;
    }

    @Override
    public void run() {
        super.run();
        try {
            switch (randomBallID) {
                case (BallFactory.GOOD_BALL_ID):
                    Thread.sleep(TIME_SHORT);
                    break;
                case (BallFactory.SILVER_BALL_ID):
                    Thread.sleep(TIME_MEDIUM);
                    break;
                default: //NORMAL_BALL_ID || BAD_BALL_ID
                    Thread.sleep(TIME_LONG);
                    break;
            }
            this.viewer.reloadBitmap();
            this.viewer.postInvalidate();
        } catch (InterruptedException e) { e.printStackTrace(); }
    }
}
