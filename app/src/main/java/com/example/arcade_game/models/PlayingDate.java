package com.example.arcade_game.models;

import android.graphics.Canvas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class PlayingDate {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static String convertDateToString(Date date) {
        if(date != null) return dateFormat.format(date);
        else return "";
    }

    public Date convertStringToDate(String dateString) throws ParseException {
        Date date = dateFormat.parse(dateString);
        return date;
    }

//    private Date getTodayDate() {
//        return Calendar.getInstance().getTime();
//    }

    public static String getTodayDate() {
        return convertDateToString(Calendar.getInstance().getTime());
    }
}
