package com.example.arcade_game.models;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;

public class GamePaint extends Paint {

    private View viewer;
    private Canvas canvas;
    private String labelPoints = "Points:" + Score.points;
    private String labelTimeLeft = "Time:" + GameCountDownTimer.timeLeft;
    private final String LABEL_COORDINATE_X = "X=";
    private final String LABEL_COORDINATE_Y = "Y=";

    public GamePaint(View viewer, Canvas canvas) {
        this.viewer = viewer;
        this.canvas = canvas;
    }

    public void drawTimeLeft() {
        this.setProperties(Color.BLUE, 70, Typeface.SERIF, Typeface.ITALIC);
        this.drawTextTopLeft(labelTimeLeft);
    }

    public void drawTimeLeftWithWarning() {
        this.setProperties(Color.RED, 105, Typeface.SERIF, Typeface.ITALIC);
        this.drawTextTopLeft(labelTimeLeft);
    }

    public void drawPoints() {
        this.setProperties(Color.BLUE, 70, Typeface.SERIF, Typeface.ITALIC);
        this.drawTextTopRight(labelPoints);
    }

    public void drawCoordinateX(float x) {
        this.setProperties(Color.CYAN, 60, Typeface.SERIF, Typeface.NORMAL);
        this.drawTextBottomLeft(LABEL_COORDINATE_X + x);
    }

    public void drawCoordinateY(float y) {
        this.setProperties(Color.CYAN, 60, Typeface.SERIF, Typeface.NORMAL);
        this.drawTextBottomRight(LABEL_COORDINATE_Y + y);
    }

    public void drawBackground(Context context) {
        this.canvas.drawColor(GameSharedPreferences.getMapColor(context));
    }

    private void drawTextTopLeft(String text) {
        float txtHeight = this.getTextSize();
        this.canvas.drawText(text, 0, txtHeight, this);
    }
    private void drawTextTopRight(String text) {
        final int maxX = viewer.getWidth();
        float txtHeight = this.getTextSize();
        float txtWidth = this.measureText(text);
        this.canvas.drawText(text, maxX - txtWidth, txtHeight, this);
    }
    private void drawTextBottomLeft(String text) {
        final int maxY = viewer.getHeight();
        this.canvas.drawText(text, 60, maxY,	this);
    }
    private void drawTextBottomRight(String text) {
        final int maxX = viewer.getWidth();
        final int maxY = viewer.getHeight();
        float txtWidth = this.measureText(text);
        this.canvas.drawText(text, maxX	- txtWidth - 60, maxY, this);
    }

    private void setProperties(int textColor, int textSize, Typeface font, int fontStyle) {
        this.setColor(textColor);
        this.setTextSize(textSize);
        this.setTypeface(Typeface.create(font, fontStyle));
    }

    public void drawBitmap(BallFactory bitmap) {
        this.canvas.drawBitmap(bitmap.getRandomBall(), bitmap.getCoordinates()[0], bitmap.getCoordinates()[1], this);
    }
}